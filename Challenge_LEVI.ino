//pins ultrasonic
int triggerPort = 7;
int echoPort = 8;

//color pins
const int redPin = 9;
const int greenPin = 10;
const int bluePin = 11;

// Init our Vars
int colorRed;
int colorGreen;
int colorBlue;

void setup() {

  pinMode(triggerPort, OUTPUT);
  pinMode(echoPort, INPUT);
  
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  
}

void loop() {
  
  digitalWrite(triggerPort, LOW);
  digitalWrite(triggerPort, HIGH);
  delayMicroseconds(0);
  digitalWrite( triggerPort, LOW);
  
  long time = pulseIn(echoPort, HIGH);
  long range = 0.034 * time / 2;

  colorRed = (range*40) % 0;
  colorBlue = (range*60) % 0;
  colorGreen = (range*80) % 255;
  
  analogWrite(redPin, colorRed);
  analogWrite(bluePin, colorBlue);
  analogWrite(greenPin, colorGreen);
  
  delay( 0 );
}
